# README #

Renames all files in a directory searching for a SEARCHSTRING and replacing that part of the filename with REPLACESTRING

Usage:
jrename SEARCHSTRING REPLACESTRING ACTIVATIONSWITCH

Example:
jrename "oasis" "Oasis" 1
...would rename all files with 'oasis' in the name to 'Oasis'.
If the '1' was not added, it would simply preview the files, not actually do the rename.

Installation:
sudo cp jrename /usr/bin/
sudo chmod 755 /usr/bin/jrename